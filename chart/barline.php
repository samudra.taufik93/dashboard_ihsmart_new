<!-- Styles -->
<style>
#chartdiv {
  width: 100%;
  height: 390px;
}
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<?php
    include '../config/db.php';
    $data_points = array();
    
    $result = mysqli_query($link, "select 
    case bulan when 1 then 'Jan'
    when 2 then'Feb' when 3 then 'Mar'
    when 4 then 'Apr'
    when 5 then 'May'
    when 6 then 'Jun'
    when 7 then 'Jul'
    when 8 then 'Aug'
    when 9 then 'Sep' end as bulan, jml, jml*300000/1000000 income  from 
  (select 6 as bulan , 5 as jml
  union ALL
  select 7, 10
  union all 
  select month(time_insert), count(*) jml from master_camera where status='active' group by MONTH(time_insert)
  )a");
    
    while($row = mysqli_fetch_array($result))
    {        
        $point = array("year" => $row['bulan'] , "income"=> $row['income'], "active_users" => $row['jml']);
        
        array_push($data_points, $point);        
    }
    $point_proyeksi = array("year" => "Next_Month" , "income"=> 10, "active_users" => 25,  "strokeWidth"=> 1,
    "columnDash"=> "5,5",
    "fillOpacity"=>0.2,
    "additional"=> "(projection)"
   );
    array_push($data_points, $point_proyeksi);    
?>
<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Export
chart.exporting.menu = new am4core.ExportMenu();

// Data for both series
var data = <?php echo json_encode($data_points, JSON_NUMERIC_CHECK); ?>




/* Create axes */
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "year";
categoryAxis.renderer.minGridDistance = 30;

/* Create value axis */
var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

/* Create series */
var columnSeries = chart.series.push(new am4charts.ColumnSeries());
columnSeries.name = "Income";
columnSeries.dataFields.valueY = "income";
columnSeries.dataFields.categoryX = "year";

columnSeries.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY} Juta Rupiah[/] [#fff]{additional}[/]"
columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
columnSeries.columns.template.propertyFields.stroke = "stroke";
columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
columnSeries.tooltip.label.textAlign = "middle";

var lineSeries = chart.series.push(new am4charts.LineSeries());
lineSeries.name = "active_users";
lineSeries.dataFields.valueY = "active_users";
lineSeries.dataFields.categoryX = "year";

lineSeries.stroke = am4core.color("#fdd400");
lineSeries.strokeWidth = 3;
lineSeries.propertyFields.strokeDasharray = "lineDash";
lineSeries.tooltip.label.textAlign = "middle";

var bullet = lineSeries.bullets.push(new am4charts.Bullet());
bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
bullet.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
var circle = bullet.createChild(am4core.Circle);
circle.radius = 4;
circle.fill = am4core.color("#fff");
circle.strokeWidth = 3;

chart.data = data;

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>