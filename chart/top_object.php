<!-- Styles -->
<style>
#chartdiv {
  width: 100%;
  height: 400px;
}

</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv", am4charts.XYChart);
chart.padding(40, 40, 40, 40);

var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "network";
categoryAxis.renderer.minGridDistance = 1;
categoryAxis.renderer.inversed = true;
categoryAxis.renderer.grid.template.disabled = true;

var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;

var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.categoryY = "network";
series.dataFields.valueX = "MAU";
series.tooltipText = "{valueX.value}"
series.columns.template.strokeOpacity = 0;
series.columns.template.column.cornerRadiusBottomRight = 5;
series.columns.template.column.cornerRadiusTopRight = 5;

var labelBullet = series.bullets.push(new am4charts.LabelBullet())
labelBullet.label.horizontalCenter = "left";
labelBullet.label.dx = 10;
labelBullet.label.text = "{values.valueX.workingValue.formatNumber('#.0as')}";
labelBullet.locationX = 1;

// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
series.columns.template.adapter.add("fill", function(fill, target){
  return chart.colors.getIndex(target.dataItem.index);
});


<?php
    include '../config/db.php';
    $data_points = array();
    
    $result = mysqli_query($link, "select * from (
      select 'car' nama, sum(car) jml from video
      where date(`timestamp`)= (select max(date(`timestamp`)) from video)
      union all 
      select 'bicycle' nama, sum(bicycle)from video
      where date(`timestamp`)= (select max(date(`timestamp`)) from video)
      union all
      select 'motorbike' nama, sum(motorbike)from video
      where date(`timestamp`)= (select max(date(`timestamp`)) from video)
      union all
      select 'person' nama, sum(person)from video
      where date(`timestamp`)= (select max(date(`timestamp`)) from video)
      )x order by 2 desc
    ");
    
    while($row = mysqli_fetch_array($result))
    {        
        $point = array("network" => $row['nama'] , "MAU"=> $row['jml']);
        
        array_push($data_points, $point);        
    }
  
?>

categoryAxis.sortBySeries = series;
chart.data = <?php echo json_encode($data_points, JSON_NUMERIC_CHECK); ?>

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>
