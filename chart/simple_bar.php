<?php

    include '../config/db.php';
    $data_points = array();
    
    $result = mysqli_query($link, "select SUBSTRING(`timestamp`,1,10) tgl,concat('Jam ',SUBSTRING(`timestamp`,12,2) ,'.00') jam,count(*) total  from video group by 1,2
	order by 1,2 limit 24");
    
    while($row = mysqli_fetch_array($result))
    {        
        $point = array("y" => $row['total'] , "label"=> $row['jam']);
        
        array_push($data_points, $point);        
    }

 
?>
<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function() {
 
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	title:{
	},
	axisY: {
		title: "Total"
	},
	data: [{
        color: "#146DA7",  
		type: "column",
		yValueFormatString: "#,##0.## hit",
		dataPoints: <?php echo json_encode($data_points, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
 
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 350px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>                              