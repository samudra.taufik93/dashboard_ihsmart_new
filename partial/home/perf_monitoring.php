<?php
include 'config/db.php';
$sql3 ="select jumlah_user,jumlah_user_bulan_ini,round(100*jumlah_user_bulan_ini/jumlah_user,2) persen_bulan_ini,
jumlah_user_active,jumlah_user_active_bulan_ini, round(100*jumlah_user_active_bulan_ini/jumlah_user_active,2) persen_active_bulan_ini
from 
(select 1,count(*)jumlah_user_active from 
(select distinct msisdn from master_camera where status='active')a)a
left join 
(select 1,count(*)jumlah_user from 
(select distinct msisdn from master_camera)a)b
on a.1=b.1
left join 
(select 1,count(*)jumlah_user_bulan_ini from 
(select distinct msisdn from master_camera where status='active' and month(time_insert)=month(CURRENT_DATE) )a)c
on a.1=c.1
left join 
(select 1,count(*)jumlah_user_active_bulan_ini from 
(select distinct msisdn from master_camera where month(time_insert)=month(CURRENT_DATE) )a)d
on a.1=d.1
";
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}
?>
<div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Performance Monitoring</h5>
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-white active">Today</button>
                                        <button type="button" class="btn btn-xs btn-white">Monthly</button>
                                        <button type="button" class="btn btn-xs btn-white">Annual</button>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart" style="height:400px;">
                                    <iframe src="<?php include "config/baseurl.php" ?>chart/barline.php"
                            name="thumbnails"
                            frameborder="0" scrolling="no" 
                            style="width: 100%; height: 370px; overflow:hidden; position:relative; left:-30px;top:-20px;"> </iframe>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                    
                                     <?php $query = $link->query($sql3);
                                    while ( $data = $query->fetch_assoc()) 
                                        { $cur_month=$data['jumlah_user_bulan_ini'];
                                         $total_month=$data['jumlah_user'];
                                         $persen=$data['persen_bulan_ini'];$persen_active=$data['persen_active_bulan_ini'];
                                         $income=$cur_month*300000;
                                        }
                                         ?>
                                        <li>
                                            <h2 class="no-margins"><?php echo $total_month;?></h2>
                                            <small>Total Active User</small>
                                            <div class="stat-percent"><?php echo $persen;?>% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: <?php echo $persen;?>%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins "><?php echo $cur_month;?></h2>
                                            <small>Total Active User in Periode</small>
                                            <div class="stat-percent"><?php echo $persen_active;?>% <i class="fa fa-level-down text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width:  <?php echo $persen_active;?>%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins "><?php echo rupiah($income);?></h2>
                                            <small>Monthly income from active users</small>
                                            <div class="stat-percent">% <i class="fa fa-bolt text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 22%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
                    