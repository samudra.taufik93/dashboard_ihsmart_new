<?php
include 'config/db.php';
$sql = "select *, round(100*jumlah_bulan_ini/jumlah_total,2) persen from   
(select 1, count(*) jumlah_bulan_ini from master_camera
where month(time_insert)=month(CURRENT_DATE))a 
left join 
(select 1, count(*) jumlah_total from master_camera )b
on a.1=b.1";
$sql2 = "
select *, round(100*jumlah_bulan_ini/jumlah_total,2) persen from   
(select 1, count(*) jumlah_bulan_ini from master_camera
where month(time_insert)=month(CURRENT_DATE) and status='active')a 
left join 
(select 1, count(*) jumlah_total from master_camera where status='active')b
on a.1=b.1";
$sql3 ="select jumlah_user,jumlah_user_bulan_ini,round(100*jumlah_user_bulan_ini/jumlah_user,2) persen_bulan_ini,
jumlah_user_active,jumlah_user_active_bulan_ini, round(100*jumlah_user_active_bulan_ini/jumlah_user_active,2) persen_active_bulan_ini
from 
(select 1,count(*)jumlah_user_active from 
(select distinct msisdn from master_camera where status='active')a)a
left join 
(select 1,count(*)jumlah_user from 
(select distinct msisdn from master_camera)a)b
on a.1=b.1
left join 
(select 1,count(*)jumlah_user_bulan_ini from 
(select distinct msisdn from master_camera where status='active' and month(time_insert)=month(CURRENT_DATE) )a)c
on a.1=c.1
left join 
(select 1,count(*)jumlah_user_active_bulan_ini from 
(select distinct msisdn from master_camera where month(time_insert)=month(CURRENT_DATE) )a)d
on a.1=d.1
";
$sql4="select *, round(100*jumlah_active/jumlah_total,2) persen from   
(select 1, count(*) jumlah_active from master_camera
where status='active')a 
left join 
(select 1, count(*) jumlah_total from master_camera)b
on a.1=b.1";

$sql5="select *, round(100*jml_notif/jml_notif_hr_ini,2) persen from   
(select 1, count(*) jml_notif from video)a 
left join 
(select 1, count(*) jml_notif_hr_ini from video where date(time_insert)=CURRENT_DATE)b
on a.1=b.1
";
?>


<div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Total Monthly</span>
                                <h5>Total User</h5>
                            </div>
                            <?php $query = $link->query($sql3);
                                    while ( $data = $query->fetch_assoc()) 
                                        { $cur_month=$data['jumlah_user_bulan_ini'];
                                         $total_month=$data['jumlah_user'];
                                         $persen=$data['persen_bulan_ini'];
                                        }
                                         ?>
    
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $total_month;?></h1>
                                <div class="stat-percent font-bold text-success">
                                <?php echo $cur_month;?>&nbsp;(<?php echo $persen;?>)% <i class="fa fa-bolt"></i></div>
                                <small>Total User</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Total Monthly</span>
                                <h5>Active User</h5>
                            </div>
                            <?php $query = $link->query($sql3);
                                    while ( $data = $query->fetch_assoc()) 
                                        { $cur_month=$data['jumlah_user_active_bulan_ini'];
                                         $total_month=$data['jumlah_user_active'];
                                         $persen=$data['persen_active_bulan_ini'];
                                        }
                                         ?>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $total_month;?></h1>
                                <div class="stat-percent font-bold text-info"><?php echo $cur_month;?>&nbsp;(<?php echo $persen;?>)%  <i class="fa fa-level-up"></i></div>
                                <small>Total Active User</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total Active Cam</span>
                                <h5>Camera</h5>
                            </div><?php
                                    $query = $link->query($sql4);
                                    while ( $data = $query->fetch_assoc()) 
                                        { $cur_month=$data['jumlah_active'];
                                         $total_month=$data['jumlah_total'];
                                         $persen=$data['persen'];
                                        }
                                         ?>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $total_month;?></h1>
                                <div class="stat-percent font-bold text-navy"><?php echo $cur_month;?>&nbsp;(<?php echo $persen;?>)% <i class="fa fa-level-up"></i></div>
                                <small>Total Camera</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                                 <div class="ibox-title">
                   <div class="ibox float-e-margins">
                                <span class="label label-danger pull-right">Alert Today</span>
                                <h5>Alert Notif</h5>
                            </div>
                            <?php
                                    $query = $link->query($sql5);
                                    while ( $data = $query->fetch_assoc()) 
                                        { $cur_month=$data['jml_notif_hr_ini'];
                                         $total_month=$data['jml_notif'];
                                         $persen=$data['persen'];
                                        }
                                         ?>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $total_month;?></h1>
                                <div class="stat-percent font-bold text-danger"><?php echo $cur_month;?>&nbsp;(<?php echo $persen;?>)%<i class="fa fa-level-down"></i></div>
                                <small>Total ALert</small>
                            </div>
                        </div>
            </div>
