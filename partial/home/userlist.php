<?php
include 'config/db.php';
$sql ="select COALESCE(camera_name,concat('User ',camera_id)) user_n,camera_id,concat(left (msisdn,5),'xxxxxxx') msisdn, time_insert,
round((UNIX_TIMESTAMP(CONVERT_TZ(CURRENT_TIMESTAMP,'GMT','Asia/Bangkok'))-UNIX_TIMESTAMP(CONVERT_TZ(time_insert,'GMT','Asia/Bangkok')))/60,2) menit 
from master_camera order by time_insert desc limit 5
";

?>

<div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>New Active User</h5>
                                        <div class="ibox-tools">
                                            <span class="label label-warning-light pull-right">10 New</span>
                                           </div>
                                    </div>
                                    <div class="ibox-content">

                                        <div>
                                            <div class="feed-activity-list">
                                             
                                     <?php $query = $link->query($sql);
                                    while ( $data = $query->fetch_assoc()) 
                                        { 
                                         ?>
                                                <div class="feed-element">
                                                    <a href="#" class="pull-left">
                                                        <img alt="image" class="img-circle" src="img/useri.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-right"><?php echo $data['menit']; ?> mins ago</small>
                                                        <strong><?php echo $data['user_n']; ?></strong> <?php echo $data['msisdn']; ?><br>
                                                        <small class="text-muted"><?php echo $data['time_insert']; ?></small>

                                                    </div>
                                                </div>
                                                <?php }?>
             </div>
                                            <a href="<?php include "config/baseurl.php" ?>users.php"> 
                                            <button class="btn btn-primary btn-block m-t"><i class="fa fa-arrow-down"></i>Show More</button> </a>

                                        </div>

                                    </div>
                                </div>

                               