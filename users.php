<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.9.2
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PICASO - OD</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/tab.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="icon" 
      type="image/png" 
      href="favicon.ico" />

</head>

<body class="pace-done mini-navbar">

    <div id="wrapper">
 
<?php include "partial/cek.php" ?>
    <?php include "partial/header.php";?>
    
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data List user camera</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                        </li>
                   
                        <li class="breadcrumb-item active">
                            <strong>Users</strong>
                        </li>
                    </ol>
                </div>
            </div>



    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

            <div class="col-lg-12" id="list">
        
        <div class="ibox">
            <div class="ibox-content">
       
                <div class="col-sm-3" style="position:relative; left:-1%;">
                    <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                </div>
                <br><br>

                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                    <thead>
                    <tr>
                        
                    <th data-toggle="true">Camera</th>
                        <th data-toggle="true">Id</th>
                        <th data-hide="true">Model</th>
                        <th data-hide="all">user msisdn</th>
                        
                        <th data-hide="all">timestamp</th>
                        <th data-hide="phone">Status Camera</th>
                        <th class="text-right" data-sort-ignore="true">Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    $link = mysqli_connect("ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com", "admin", "Dsc5#57Vx", "ihsmart_dev");
                    $sql = "SELECT * from master_camera order by 1";
                    $query = $link->query($sql);
                    while ( $data = $query->fetch_assoc()) 
                        {
                         
	                ?>
  


                        
                    <tr>
                        <td>
                        <img src="img/ezvis.jpg" width="80" height="60">
                        </td>
                        <td>
                         <?php echo $data['camera_id'];?>
                        </td>
                        <td>
                         <?php echo $data['camera_name'];?>
                        </td>
                        <td>
                       
                        <?php echo $data['msisdn'];?>
                        </td>
                        
                        <td>
                       
                        <?php echo $data['lastime_active'];?>
                        </td>
                   
                        <td>
                            <span 
                           class=" <?php if (  $p->status =='active') echo "label label-primary";
                           elseif (  $p->status =='inactive') echo "label label-danger";
                           else echo "label label-warning";
                           
                           ?>"

                                                                              
                           >
                           
                            <?php echo $data['status'];?>
                            </span>
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <button class="btn-white btn btn-xs" onclick="location.href='#'">
                                View
                                <img src="https://img.icons8.com/small/16/000000/video-call.png"/>
                               
                                
                                <button class="btn-white btn btn-xs" data-toggle="modal" data-target="#myModal{{ $p->id_camera }}">Config
                                <img src="https://img.icons8.com/small/16/000000/wrench.png"/>
                                
                                </button>
                                
                                        <div class="modal inmodal" id="myModal{{ $p->id_camera }}" tabindex="-1" role="dialog" aria-hidden="true">

                                            <div class="modal-dialog">
                                            <div class="modal-content animated bounceInRight">
                                                    <div class="modal-header" style="text-align: center;">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <img src="https://img.icons8.com/doodle/96/000000/bullet-camera--v1.png"/>
                                                        <h4 class="modal-title">Config Camera</h4>
                                                        
                                                        <small class="font-bold">Camera_id:  <?php echo $data['camera_id'];?>,  <?php echo $data['msisdn'];?></small>
                                                        <div class="switch"  style="text-align: left; position:relative;left:45%">
                                                            <div class="onoffswitch" style="text-align: left;justify-content: center;align-items: center;">
                                                                <input type="checkbox" checked class="onoffswitch-checkbox" id="example1">
                                                                <label class="onoffswitch-label" for="example1">
                                                                    <span class="onoffswitch-inner"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-body">
                                                    <form action="aksi_object.php?id={{ $p->id_camera }}" method="post">
                                                    <div class="form-group row" style="text-align: left;"><label class="col-sm-6 col-form-label" style=" font-weight: bold;"> Select Object for Detection</label>

                                                        <div class="col-sm-10">
                                                        <div class="i-checks"><label> <input type="checkbox" name="object[]" value="car" <?php if(($p->flag_car)=='1') echo 'checked=""';?> > <i></i>Car </label></div>
                                                        <div class="i-checks"><label> <input type="checkbox" name="object[]" value="motorbike"  <?php if(($p->flag_motorbike)=='1') echo 'checked=""';?> > <i></i>Motorbike</label></div>
                                                        <div class="i-checks"><label> <input type="checkbox" name="object[]" value="bicycle"  <?php if(($p->flag_bicycle)=='1') echo 'checked=""';?> > <i></i>Bicycle</label></div>
                                                        <div class="i-checks"><label> <input type="checkbox" name="object[]" value="laptop"  <?php if(($p->flag_laptop)=='1') echo 'checked=""';?> > <i></i>Laptop</label></div>
                                                        <div class="i-checks"><label> <input type="checkbox" name="object[]" value="people"  <?php if(($p->flag_people)=='1') echo 'checked=""';?> > <i></i>People</label></div>
                                                        <div class="i-checks"><label> <input type="checkbox" name="object[]" value="tv"  <?php if(($p->flag_tv)=='1') echo 'checked=""';?> > <i></i>Tv</label></div>
                                                        
                                                        </div>
                                                    </div>
                                                
                      
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                        <button type="submit" name="Submit" value="Add" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                                                   </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="6">
                            <ul class="pagination float-right"></ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>



              
            </div>
</div>
        





		
		
		
		
		
        <div class="footer">
            <div class="float-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> PT Telkom Indonesia Tbk. &copy; 2019-2022
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/jeditable/jquery.jeditable.js"></script>

    <!-- Data Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="js/plugins/dataTables/dataTables.tableTools.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <style>

    </style>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                "dom": 'lTfigt',
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                }
            });

            /* Init DataTables */
            var oTable = $('#editable').DataTable();

            /* Apply the jEditable handlers to the table */
            oTable.$('td').editable( '../example_ajax.html', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );


        });

        function fnClickAddRow() {
            $('#editable').dataTable().fnAddData( [
                "Custom row",
                "New row",
                "New row",
                "New row",
                "New row" ] );

        }
    </script>
    
</body>
</html>
