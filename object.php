<!DOCTYPE html>
<html lang="en">
<head>
<title>Pagination</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<?php  
//Change the password to match your configuration
  $link = mysqli_connect("ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com", "admin", "Dsc5#57Vx", "ihsmart_dev");

  // Check connection
  if($link === false){
      die("ERROR: Could not connect. " . mysqli_connect_error());
  }
  
  
?>

<div class="col-sm-6" style="padding-top: 20px; padding-bottom: 20px;">
<h3>DataTabels dengan PHP dan Bootstrap</h3>
<hr>

<table class="table table-stripped table-hover datatab">
<thead>
<tr>
				<th>No</th>
                <th>Timestamp</th>
                <th>Obj Name</th>
                <th>Count_before</th>
                <th>Count_after</th>
                <th>Image_before</th>
				 <th>Image_after</th>                      
</tr>
</thead>  
<tbody>
<?php 
	
	      $sql = "SELECT * from object order by timestamp desc limit 100";
          $query = $link->query($sql);
          $no = 1;
          while ($data = $query->fetch_assoc()) 
{
?>
<tr>

              <td><?php echo $no++; ?></td>
			  <td><?php echo $data['timestamp']; ?></td>   
              <td><?php echo $data['object_name']; ?></td>         
              <td><?php echo $data['count_before']; ?></td>
              <td><?php echo $data['count_after']; ?></td>
			  <td>
<!-- Button untuk modal -->
<a href="#" type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal<?php echo $data['object_id']; ?>">
 <img src="<?php echo $data['image_url_before']; ?>" alt="Girl in a jacket" width="500" height="200"></td>
</a>
</td>
<td>
<!-- Button untuk modal -->
<a href="#" type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal<?php echo $data['image_url_after']; ?>">
 <img src="<?php echo $data['image_url_after']; ?>" alt="Girl in a jacket" width="500" height="200"></td>
</a>
</td>
</tr>
<!-- Modal Edit Mahasiswa-->
<div class="modal fade" id="myModal<?php echo $data['object_id']; ?>" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content" style="width:165%;position:relative;left:-20%">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Image Detail</h4>
</div>
<div class="modal-body">
    <?php ?>
 <img src="<?php echo $data['image_url_before'];?>" alt="Girl in a jacket" width="900" height="500">

<div class="modal-footer">  
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

   
</div>
</div>

</div>
</div>
<!-- Modal Edit after-->
<div class="modal fade" id="myModal<?php echo $data['image_url_after']; ?>" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content" style="width:165%;position:relative;left:-20%">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Image Detail</h4>
</div>
<div class="modal-body">
    <?php ?>
 <img src="<?php echo $data['image_url_after'];?>" alt="Girl in a jacket" width="900" height="500">

<div class="modal-footer">  
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

   
</div>
</div>

</div>
</div>
<?php               
} 
?>
</tbody>
</table>          
</div>

</body>
<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
$('.datatab').DataTable();
} );
</script>
</html>