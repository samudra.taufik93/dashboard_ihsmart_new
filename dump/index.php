<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Data Tables</title>
  
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  
  <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
  <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="icon" type="image/png" href="favicon.ico">
  
  
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  
  <head>
  <title>Object Detection Raw Data</title>
</head>
</head>
<body>
<h1>Object Detection Raw Data</h1>
<br>

                 
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Please Select Camera id..
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
	  <?php
	$link = mysqli_connect("ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com", "admin", "Dsc5#57Vx", "ihsmart_dev");
	$sql = "SELECT distinct camera_id from object order by 1";
	$query = $link->query($sql);
	 while ($data = $query->fetch_assoc()) 
          {
			echo ' <li><a class="dropdown-item" href="index2.php?id='.$data['camera_id'].'">'.$data['camera_id'].'</a></li>';
		}
	
	?>
    </ul>
  </div>

<br>


<br>

  <?php
   //Change the password to match your configuration
 $link = mysqli_connect("ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com", "admin", "Dsc5#57Vx", "ihsmart_dev");
	

  // Check connection
  if($link === false){
      die("ERROR: Could not connect. " . mysqli_connect_error());
  }

  ?>
  <table id="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <thead>
            <tr>
			 <th>No</th>
                <th>camera_id</th>
                <th>Timestamp</th>
                <th>Obj Name</th>
                <th>Count_before</th>
                <th>Count_after</th>
				 <th>Selisih</th>
                <th>Image_before</th>
				 <th>Image_after</th>
            </tr>
        </thead>
        <tfoot>
          <tr>
                <th>No</th>
				 <th>camera_id</th>
                <th>Timestamp</th>
                <th>Obj Name</th>
                <th>Count_before</th>
                <th>Count_after</th>
				 <th>Selisih</th>
                <th>Image_before</th>
				 <th>Image_after</th>
            </tr>
        </tfoot>
        <tbody>
           <?php 
		  $sql = "SELECT *,UNIX_TIMESTAMP(timestamp)-UNIX_TIMESTAMP(CONVERT_TZ(time_sent,'GMT','Asia/Bangkok'))   selisih from object where camera_id=1 order by timestamp desc limit 100";
          $query = $link->query($sql);
          $no = 1;
          while ($data = $query->fetch_assoc()) 
          {
          ?>
            <tr>
              <td><?php echo $no++; ?></td> <td><?php echo$data['camera_id']; ?></td>
			  <td><?php echo $data['timestamp']; ?></td>   
              <td><?php echo $data['object_name']; ?></td>         
              <td><?php echo $data['count_before']; ?></td>
              <td><?php echo $data['count_after']; ?></td>
			    <td><?php echo $data['selisih']; ?></td>
			  <td style="  text-align: center;
  vertical-align: middle;">
			  <a href="<?php echo $data['image_url_before']; ?>">
			  <img src="<?php echo $data['image_url_before']; ?>" alt="Girl in a jacket" width="500" height="200"></td>
			    <td style="  text-align: center;
  vertical-align: middle;" >
			  <a href="<?php echo $data['image_url_after']; ?>">
			  <img src="<?php echo $data['image_url_after']; ?>" alt="Girl in a jacket" width="500" height="200"></td>
            </tr>
          <?php               
          } 
          ?>
          
        </tbody>
    </table>
  
  <script>
  $(document).ready(function(){
    $('#tabel-data').DataTable();
});
  </script>

  
</body>
</html>