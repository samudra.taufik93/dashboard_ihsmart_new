<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Data Tables</title>
  
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  
  <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
  <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="icon" type="image/png" href="favicon.ico">
  
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <style>
.container {
  position: relative;
  width: 100%;
  max-width: 500px;
}

.container img {
  width: 100%;
  height: auto;
}

.container .btn {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  background-color: #555;
  color: white;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;
  text-align: center;
}

.container .btn:hover {
  background-color: black;
}
</style>
  
  <head>
  <title>Object Detection Raw Data</title>
</head>
</head>
<body>
<h1>Object Detection Raw Data</h1>
<br>

                 
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Please Select Camera id..
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
	  <?php
	$link = mysqli_connect("ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com", "admin", "Dsc5#57Vx", "ihsmart_dev");
	$sql = "SELECT distinct camera_id from object order by 1";
	$query = $link->query($sql);
	 while ($data = $query->fetch_assoc()) 
          {
			echo ' <li><a class="dropdown-item" href="index2.php?id='.$data['camera_id'].'">'.$data['camera_id'].'</a></li>';
		}
	
	?>
    </ul>
  </div>

<br>


<br>

  <?php
   //Change the password to match your configuration
 $link = mysqli_connect("ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com", "admin", "Dsc5#57Vx", "ihsmart_dev");
	

  // Check connection
  if($link === false){
      die("ERROR: Could not connect. " . mysqli_connect_error());
  }

  ?>
  <table id="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <thead>
            <tr>
		            <th>No</th>
				 <th>camera_id</th>
                <th>Timestamp</th>
                <th>video Name</th><th>object list</th>
                <th>motorbike</th>
                <th>car</th>
				 <th>person</th>
				  <th>bicycle</th>
				 
                <th>Image_capture</th>  <th>play_video</th>
            </tr>
        </thead>
        <tfoot>
          <tr>
                <th>No</th>
				 <th>camera_id</th>
                <th>Timestamp</th>
                <th>video Name</th><th>bject list</th>
				
                <th>motorbike</th>
                <th>car</th>
				 <th>person</th>
				  <th>bicycle</th>
				 
                <th>Image_before</th> <th>play_video</th>
            </tr>
        </tfoot>
        <tbody>
           <?php 
		  $sql = "select DISTINCT *,frame image_url_before, @rownum:=@rownum+1 object_id from video,(SELECT @rownum:=0) r order by timestamp desc";
          $query = $link->query($sql);
          $no = 1;
          while ($data = $query->fetch_assoc()) 
          { $motor= $data['motorbike'];$car=$data['car'];$person=$data['person'];$bicycle=$data['bicycle'];
          ?>
            <tr>
              <td><?php echo $no++; ?></td> <td><?php echo$data['camera_id']; ?></td>
			  <td><?php echo $data['timestamp']; ?></td>   
              <td><?php echo $data['video_id']; ?></td>   
					
			<td><?php if ($motor>0) {echo 'motorbike ';} 
			if ($car>0){echo 'car ';}
			if ($person>0) {echo 'person ';}
			if ($bicycle>0 ){echo 'Bicycle ';} ?>
			</td> 		  
              <td><?php echo $data['motorbike']; ?></td>
              <td><?php echo $data['car']; ?></td>
			    <td><?php echo $data['person']; ?></td>    <td><?php echo $data['bicycle']; ?></td>
			  <td style="  text-align: center;vertical-align: middle;">
			  
				<!-- Button untuk modal -->
				<a href="#" data-toggle="modal" data-target="#myModal<?php echo $data['object_id']; ?>">
				
				<div class="container">
				  <img src="<?php echo $data['image_url_before'];?>" alt="Girl in a jacket" width="500" height="200">
				</div>
				</a>
				
				
				<!-- Modal Edit Mahasiswa-->
				<div class="modal fade" id="myModal<?php echo $data['object_id']; ?>" role="dialog">
				<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content" style="width:165%;position:relative;left:-20%">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Image Detail</h4>
				</div>
				<div class="modal-body">
					<?php ?>
				 <img src="<?php echo $data['image_url_before'];?>" alt="Girl in a jacket" width="900" height="500">

				<div class="modal-footer">  
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

				   
				</div>
				</div>

				</div>
				</div>
 
			  </td>
			  
			  <td style="  text-align: center;vertical-align: middle;">
			  
				<!-- Button untuk modal -->
				<a href="#" data-toggle="modal" data-target="#myModalvid<?php echo $data['object_id']; ?>">
			
<div class="container">
  <img src="<?php echo $data['image_url_before'];?>" alt="Girl in a jacket" width="500" height="200">
  <button class="btn">Play Video ▶</button>
</div>
				
				</td>
				</a>
				
				
				<!-- Modal Edit Mahasiswa-->
				<div class="modal fade" id="myModalvid<?php echo $data['object_id']; ?>" role="dialog">
				<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content" style="width:165%;position:relative;left:-20%">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Image Detail</h4>
				</div>
				<div class="modal-body">
					
				<video width="900" controls autoplay>
				  <source src="<?php echo $data['video_link'];?>" type="video/mp4">
				  Your browser does not support HTML video.
				</video>
				</div>

				<div class="modal-footer">  
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

				   
				</div>
				</div>

				</div>
				</div>
			  
			  
			  
			  
			  

			  
			  </td>
			
			
            </tr>
          <?php               
          } 
          ?>
          
        </tbody>
    </table>
  
  <script>
  $(document).ready(function(){
    $('#tabel-data').DataTable();
});
  </script>

  
</body>
</html>