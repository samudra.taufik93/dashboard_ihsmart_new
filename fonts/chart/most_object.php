<!-- Styles -->
<style>
#chartdiv {
  width: 95%;
  height: 400px;
}

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<?php 
$conn = new mysqli('10.48.102.97', 'picaso_dev', '5#57Vx', 'picaso_dev');
if ($conn->connect_error) {
	die("Connection error: " . $conn->connect_error);
}
$result = $conn->query("
select object_name name, 
case object_name 
when 'laptop' then 'https://cdn2.iconfinder.com/data/icons/ikooni-outline-seo-web/128/seo3-16-512.png'
when 'person' then 'https://cdn3.iconfinder.com/data/icons/business-management-6/341/Account_accounts_acquirer_affiliate_agreement_allience_audience_avatar_business_businessman_clients-512.png'
when 'chair' then 'https://d29fhpw069ctt2.cloudfront.net/icon/image/49065/preview.svg'
when 'backpack' then 'https://cdn4.iconfinder.com/data/icons/travel-21/256/Backpack-512.png'
when 'diningtable'then 'https://cdn3.iconfinder.com/data/icons/unigrid-phantom-interior-vol-2/60/026_076_interior_furniture_dining_table-512.png'
when 'cup' then 'https://cdn0.iconfinder.com/data/icons/food-icons-rounded/110/Tea-Cup-512.png'
else 
'https://cdn3.iconfinder.com/data/icons/unigrid-phantom-interior-vol-1/60/026_023_interior_furniture_bench-512.png' 
end as href ,count(*) steps from object_detail
where left(frame_id,8)=cast(DATE_FORMAT(curdate()-1,'%m%d%Y') as char)
group by 1 order by 3 desc
limit 10
");
?>	

<?php
$dataPoints = array();

while ( $row = $result->fetch_assoc() ) {
    $dataPoints[] = $row;
  }
 // echo json_encode( $dataPoints );
?>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

/**
 * Chart design taken from Samsung health app
 */

var chart = am4core.create("chartdiv", am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.paddingBottom = 30;

chart.data = <?php echo json_encode($dataPoints, JSON_PRETTY_PRINT) ?>;



var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "name";
categoryAxis.renderer.grid.template.strokeOpacity = 0;
categoryAxis.renderer.minGridDistance = 10;
categoryAxis.renderer.labels.template.dy = 35;
categoryAxis.renderer.tooltip.dy = 35;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.fillOpacity = 0.3;
valueAxis.renderer.grid.template.strokeOpacity = 0;
valueAxis.min = 0;
valueAxis.cursorTooltipEnabled = false;
valueAxis.renderer.baseGrid.strokeOpacity = 0;
valueAxis.title.text = "Total Object";

var series = chart.series.push(new am4charts.ColumnSeries);
series.dataFields.valueY = "steps";
series.dataFields.categoryX = "name";
series.tooltipText = "{valueY.value}";
series.tooltip.pointerOrientation = "vertical";
series.tooltip.dy = - 6;
series.columnsContainer.zIndex = 100;

var columnTemplate = series.columns.template;
columnTemplate.width = am4core.percent(50);
columnTemplate.maxWidth = 66;
columnTemplate.column.cornerRadius(60, 60, 10, 10);
columnTemplate.strokeOpacity = 0;

series.heatRules.push({ target: columnTemplate, property: "fill", dataField: "valueY", min: am4core.color("#ebe8e8"), max: am4core.color("#b7b4b4") });
series.mainContainer.mask = undefined;

var cursor = new am4charts.XYCursor();
chart.cursor = cursor;
cursor.lineX.disabled = true;
cursor.lineY.disabled = true;
cursor.behavior = "none";

var bullet = columnTemplate.createChild(am4charts.CircleBullet);
bullet.circle.radius = 30;
bullet.valign = "bottom";
bullet.align = "center";
bullet.isMeasured = true;
bullet.mouseEnabled = false;
bullet.verticalCenter = "bottom";
bullet.interactionsEnabled = false;

var hoverState = bullet.states.create("hover");
var outlineCircle = bullet.createChild(am4core.Circle);
outlineCircle.adapter.add("radius", function (radius, target) {
    var circleBullet = target.parent;
    return circleBullet.circle.pixelRadius + 10;
})

var image = bullet.createChild(am4core.Image);
image.width = 60;
image.height = 60;
image.horizontalCenter = "middle";
image.verticalCenter = "middle";
image.propertyFields.href = "href";

image.adapter.add("mask", function (mask, target) {
    var circleBullet = target.parent;
    return circleBullet.circle;
})

var previousBullet;
chart.cursor.events.on("cursorpositionchanged", function (event) {
    var dataItem = series.tooltipDataItem;

    if (dataItem.column) {
        var bullet = dataItem.column.children.getIndex(1);

        if (previousBullet && previousBullet != bullet) {
            previousBullet.isHover = false;
        }

        if (previousBullet != bullet) {

            var hs = bullet.states.getKey("hover");
            hs.properties.dy = -bullet.parent.pixelHeight + 30;
            bullet.isHover = true;

            previousBullet = bullet;
        }
    }
})

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>