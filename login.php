<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>IHS Smart| Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="icon" 
      type="image/png" 
      href="favicon.ico" />

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IHS+</h1>

            </div>

<section class="main-section">

            <h3>   
            <?php 
			   if (empty($_GET)) {}
			   else {
					$msg=$_GET['msg'];
					
					if ($msg==1) {?>
								<div class="alert alert-danger">
									<div>Username or Password invalid</div>
								</div>
							   <?php }?>
                    <?php if ($msg==2) {?>
								<div class="alert alert-danger">
									<div>Anda belum Login</div>
								</div>
								<?php } ?>
					<?php if ($msg==3) {?>
								<div class="alert alert-success">
									<div>Anda berhasil Logout</div>
								</div>
								<?php } 
			   }?>
            </h3>
         
            <p>Login in. To see it in action.</p>
            <form class="m-t" role="form" action="config/login.php" method="post">
            
                <div class="form-group">
                    <input type="username" class="form-control" placeholder="Email" required=""id="email" name="email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" id="password" name="password">
                </div>
                <button type="submit" class="btn btn-md btn-primary" style="background-color:red">Submit</button>
                <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{url('register')}}">Create an account</a>
            </form>
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>
       <!-- /.content -->
       </section>
    <!-- /.main-section -->


    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>

