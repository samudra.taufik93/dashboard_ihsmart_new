import moviepy.editor as moviepy
import wget
import time
import pymysql
import os
import datetime
import s3


while True:
	st =datetime.datetime.utcnow()
	print(st,': Process to cek video and convert, please wait..')
	try :
		rds_host  = "ihsmartdev.cuv63sq0zhdl.ap-southeast-1.rds.amazonaws.com"
		name = "admin"
		password = "Dsc5#57Vx"
		db_name = "ihsmart_dev"
		port = 3306

		conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, port=port, connect_timeout=5)
		cur = conn.cursor()

		cur.execute("SELECT video_id,video_link  FROM video where is_convert is null and LENGTH(video_link)<300 and is_process is null order by timestamp desc limit 1")
		result_count_row = cur.fetchall()
		videoid = result_count_row[0][0]
		video_link=result_count_row[0][1]

		print(videoid,video_link)

		flag='1'
		query_process= """ UPDATE video set is_process = %s WHERE video_id = %s """
		data_stg= (flag, videoid)
		cur.execute(query_process,data_stg)

		filename=wget.download(video_link)
		clip = moviepy.VideoFileClip(filename)
		clip = clip.set_fps(10)
		clip.write_videofile('output/'+filename)
		s3.upload_to_aws('output/'+filename,'ihsmartbucket',filename)

		path='https://ihsmartbucket.s3.ap-southeast-1.amazonaws.com/'+filename
		query_stg_similarity = """ UPDATE video set is_convert = %s,link_local=%s WHERE video_id = %s """
		data_stg_similarity= (flag,path, videoid)
		cur.execute(query_stg_similarity,data_stg_similarity)

		conn.commit()
		conn.close()
		os.remove(filename)
		os.remove('output/'+filename)
	except:
		st =datetime.datetime.utcnow()
		print(st,': There is no new data to process..')
		time.sleep(5)